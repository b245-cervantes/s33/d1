console.log("Rest API");
//[SEction] Javascript Synchronos meannig it only
//executes one statement at at time
console.log("hello world");
// consoe.log("Hello");

// for(let i = 0; i <= 15; i++){
//     console.log(i)
// }
console.log("i am the console.log after the for loop!")


//async and await - it lets us to proceed or execute more than
// one statements at the same time.

//[SECTION] Getting all posts
//fetch API allows us to asynchronously request for 
//a resour or data
  // fetch() method in javascript is used to requess to the server
  //and load information on the webpage

  //Syntax:
    //fetch("apiURL")
   let x = fetch("https://jsonplaceholder.typicode.com/posts");
   console.log(x);
   /** "promise is an object that represent the eventual
    * completion (or failure") of an asynchronous function
    * and its resulting value. */

   // A promise may be in one of 3 possible 
   // state: fulfilled, rejected pending.

   /*
      pending: initial states, neither fulfilled nore rejected
      response.

      fullfiled: operation was completed
      rejected: operatiom
   
   */
  //Syntax
  // fetch("apiURI")
  // .then(res =>{What will uou do with the response}})

   fetch("https://jsonplaceholder.typicode.com/posts")
   //the ".then()" method captures the response object
   // and return another promise which will be either
   //resolve or rejected.
  .then(res =>  {
   return res.json()
})
  .then(data => {
    let title = data.map(el => el.title)
    console.log(title)
  })

  // the "async" and "await" keyword to achieve asynchronous code

  async function fetchData(){
    let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
     console.log(result)
     let json = await result.json();
     console.log(json)
  }

  fetchData();

  //SECTION - Get a specific post
    // Retrieves a sepecific post following the REST API
    //(post/:id)
    //wildcard is where you can put any value, it then creates a link
    //between id parameter in the ur; amd the value provided in the url

    fetch("https://jsonplaceholder.typicode.com/posts/3", {method: "GET"})

    .then(res => res.json())
    .then(result => console.log(result))

    //[SECTION] CREATE POST
    /**Syntax :
     * fetch("apiURL",{necessaryOption})
     * .then(response => response.json())
     * .then(resutl => {codeblock})
     */

    fetch("https://jsonplaceholder.typicode.com/posts", {
        //REST API method to execute
        method: "POST",
        //Headers, it tells us the data type of the our request
        headers: {
            "Content-Type":"application/json"
        },
        // body - contains the data that will be added to the database
        // the request of the user
        body: JSON.stringify({
            title: "New Post",
            body: "Hello World",
            userId: 1,
        })
    })
    .then(res => res.json())
    .then(result => console.log(result) )

    //SECTION - updating a post
    
    fetch("https://jsonplaceholder.typicode.com/posts/5",{
        method: "PUT",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            title: "Updated post",
            body: "Hello Again",
            userID: 1
        })
    })
    .then(res => res.json())
    .then(result => console.log(result));

    //SECTION - PATCH
    // patch - properties

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method : "PATCH",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            title: "Update title"
        })
    })
    .then(res => res.json())
    .then(result => console.log(result))

    //Mini-activity
    //using the patch method, change the body property
    //of the docuent which has the id 18 to heeloo its me

    fetch("https://jsonplaceholder.typicode.com/posts/18",{
        method: "PATCH",
        headers: {
            'Content-Type':'application/json'
        },
        body: JSON.stringify({
            body: "Hello it'sme!"
        })
    })
    .then(res => res.json())
    .then(result => console.log(result))

    //SECTION - delete

    fetch("https://jsonplaceholder.typicode.com/posts/5",{
        method: "DELETE"
    })
    .then(res => res.json())
    .then(result => console.log(result))